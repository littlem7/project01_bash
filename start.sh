#!/bin/bash

USAGE="Usage: ${0} [options]

Script could iterate over given exercise script written in bash;
execute and/or print proper lines (ignoring comments)
can handle only one-line commands
Script to parse should call ./ex<exercise>/<exercise>.sh

-h|--help\t this is some help text
-e|--exercise\t number of exercise
"

# ----------------------------------------------------
# functions to colorize output
# ----------------------------------------------------
function echo_blue() {
	echo -e "\033[0;34m${1}\033[0m"
}

function echo_green() {
	echo -e "\033[1;32m${1}\033[0m"
}

function echo_red() {
	echo -e "\033[0;31m${1}\033[0m"
}

function print_line() {
	echo_green "\n${1}\n"
}

# ----------------------------------------------------
# print error message and exit with error code
# ----------------------------------------------------
function die_with_err() {
	message="${1}"
	err_code="${2}"

	echo_red "[ERROR] ${message}"
	exit "${err_code}"
}

# ----------------------------------------------------
# exit with nice output and proper exit code
# ----------------------------------------------------
function end_script() {
	echo_blue "\nThank you for making this little program very happy!"
	exit 0
}


# ----------------------------------------------------
# increment array index without values grather than 
# size of array given in second parameter
# ----------------------------------------------------
function increment_index() {
	index_to_increment="${1}"
	array_size="${2}"
	
	if [ -z "${index_to_increment}" ]; then
		die_with_err "${FUNCNAME[0]} missing index to increment" 13
	elif [ -z "${array_size}" ]; then
		die_with_err "${FUNCNAME[0]} missing array size" 12
	fi
	
	let temp="${index_to_increment}"+1
	if [ "${temp}" -eq "${array_size}" ]; then
		echo "${index_to_increment}"
	else
		echo "${temp}"
	fi
}

# ----------------------------------------------------
# decrement array index 
# without values lower than 0
# ----------------------------------------------------
function decrement_index() {
	index_to_decrement="${1}"

	if [ -z "${index_to_decrement}" ]; then
		die_with_err "${FUNCNAME[0]} missing index to decrement" 14
	fi

	let temp="${index_to_decrement}"-1
	if [ "${temp}" -lt "0" ]; then
		echo "${index_to_decrement}"
	else
		echo "${temp}"
	fi
}

# ----------------------------------------------------
# check if line from file isn't comment or empty line
# ----------------------------------------------------
function does_proper_line() {
	line="${1}"

	if [ -n "${line}" ] && [[ ! "${line}" =~ ^#.* ]]; then # ignoring empty lines & comments with shebang
		echo "1"
	else
		echo "0"
	fi
}

# ----------------------------------------------------
# get line (string) from given file 
# with index as second parameter
# ----------------------------------------------------
function get_line_from_file() {
	file_name="${1}"
	num_of_line="${2}"

	if [ -z "${file_name}" ]; then
		die_with_err "${FUNCNAME[0]} missing file name" 9
	elif [ -z "${num_of_line}" ]; then
		die_with_err "${FUNCNAME[0]} missing num of file" 8
	fi
	sed "${num_of_line}q;d" "${file_name}"
}

# ----------------------------------------------------
# die with error 11 if no such directory
# die with error 12 if script isn't executable
# ----------------------------------------------------
function check_input_values() {
	exercise_dir="${1}"
	exercise_script="${2}"

	if [ ! -d "${exercise_dir}" ]; then
		die_with_err "${exercise_dir} is not directory" 11
	elif [ ! -x "${exercise_script}" ]; then
		die_with_err "${exercise_script} should be executable" 12
	fi
}

# ----------------------------------------------------
# execute in subshell command given in parameter
# ignoring error code
# ----------------------------------------------------
function execute_line() {
	(eval "${1}" || true)
}

# ----------------------------------------------------
# get number of lines of file given in parameter
# ----------------------------------------------------
function get_num_of_lines_from_file() {
	# shellcheck disable=SC2005
	echo "$(wc -l "${1}" | awk '{print $1}')"
}

# ----------------------------------------------------
# ADDITIONAL FEATURE
# line which could be firstly execute by the script
# is first line by default
# or line after comment in format '# end of comment'
# customer could put part of code 
# which shouldn't been read before this comment
# ----------------------------------------------------
function get_begin_line_number() {
	file_name="${1}"

	if [ ! -f "${file_name}" ]; then
		die_with_err "${FUNCNAME[0]} no such file" 16
	fi

	comment_format='# end of comment'
	current_line_number=1
	IFS=$'\n' 
	while read -r -u 3 line; do
		let current_line_number="$current_line_number"+1
	   	if [ "${line}" == "${comment_format}" ]; then
			echo "${current_line_number}"
			return
		fi
	done 3<"${file_name}"
	echo "1"
}

# ----------------------------------------------------
# get list with numbers of proper lines (to execute)
# from file
# ----------------------------------------------------
function get_list_of_proper_lines() {
	file_name="${1}"

	if [ ! -f "${file_name}" ]; then
		die_with_err "${FUNCNAME[0]} no such file" 16
	fi

	declare -a lines=()
	current_line_number=$(get_begin_line_number "${file_name}")
	max_line_number=$(get_num_of_lines_from_file "${file_name}")
	while [[ "${current_line_number}" -le "${max_line_number}" ]]; do
		current_line="$(get_line_from_file "${file_name}" "${current_line_number}")"
		if [ "$(does_proper_line "${current_line}")" -eq 1 ]; then
			lines+=("${current_line_number}")
		fi
		let current_line_number="$current_line_number"+1
	done
	echo "${lines[@]}"
}

# ----------------------------------------------------
# main recurent function which print prompt message
# with current line (command)
# and could iterate over list with number of lines
# or execute current line
# ----------------------------------------------------
function main() {
	index="${1}"
	file_name="${2}"
	IFS=' ' read -r -a all_lines <<< "${3}"	# parsing string to array
	
	current_line="$(get_line_from_file  "${file_name}" "${all_lines[${index}]}")"
	print_line "${current_line}"
	prompt_message=">>> [q]uit [n]ext [p]revious [e]xecute "
	while read -r -s -n1 -p "${prompt_message}" input; do
		case "${input}" in
			"e") 
				execute_line "${current_line}"
				index="$(increment_index "${index}" "${#all_lines[@]}")"
				break
				;;
			"p")
				index="$(decrement_index "${index}")"
				break
				;;
			"n")
				index="$(increment_index "${index}" "${#all_lines[@]}")"
				break
				;;
			"q")
				end_script
				;;
			*) echo ;;
		esac
	done
	echo
	# shellcheck disable=SC2116
	main "${index}" "${file_name}" "$(echo "${all_lines[@]}")"
}

# ----------------------------------------------------
# if script isn't sourced it parse and check arguments
# and call main function
# ----------------------------------------------------
if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then

	if [[ $# -eq 0 ]]; then
		die_with_err "No arguments, try -h|--help" 1
	fi

	while [[ $# -gt 0 ]]; do
		key="${1}"
		case "$key" in
			-h|--help)
				echo -e "${USAGE}"
				exit 0
			;;
			-e|--exercise)
				exercise_num="${2}"
				shift
				exercise_dir="ex${exercise_num}"
				exercise_script="./${exercise_dir}/${exercise_num}.sh"
				check_input_values "${exercise_dir}" "${exercise_script}"
				shift
			;;
			*)
				die_with_err "Unknown option, try -h|--help" 1
			;;
		esac
	done

	export EX_DIR
	EX_DIR="$(pwd)/${exercise_dir}" # export to provide proper way of executing commands

	main "0" "${exercise_script}" "$(get_list_of_proper_lines "${exercise_script}")"

fi