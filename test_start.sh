#!/bin/bash

set +e
# shellcheck disable=SC1091
source assertions.sh
# shellcheck disable=SC1091
source start.sh


function test_die_with_error_proper() {
	dummy_error_code="3"
	dummy_error_message="dummy_message"
	(die_with_err "${dummy_error_message}" "${dummy_error_code}" &> /dev/null)
	retcode="${?}"
	assert_raises "${retcode}" "${dummy_error_code}"
}

function test_die_with_error_no_args() {
	unset message
	unset err_code
	dummy_error_code="3"
	error_code="2"
	(die_with_err "${dummy_error_code}" &> /dev/null)
	retcode="${?}"
	assert_raises "${retcode}" "${error_code}"
}

function test_die_with_error_no_second() {
	unset err_code
	error_code="2"
	dummy_error_message="dummy_message"
	(die_with_err "${dummy_error_message}" &> /dev/null)
	retcode="${?}"
	assert_raises "${retcode}" "${error_code}"
}

function test_increment_index_proper() {
	dummy_idx="2"
	array_size="4"
	proper_answer="3"
	out_idx="$(increment_index "${dummy_idx}" "${array_size}")"
	assert_equals "${out_idx}" "${proper_answer}"
}

function test_increment_index_max() {
	dummy_idx="3"
	array_size="4"
	proper_answer="3"
	out_idx="$(increment_index "${dummy_idx}" "${array_size}")"
	assert_equals "${out_idx}" "${proper_answer}"
}

function test_increment_index_missing_size() {
	unset array_size
	error_code="12"
	dummy_idx="3"
	(increment_index "${dummy_idx}" &> /dev/null)
	retcode="${?}"
	assert_raises "${retcode}" "${error_code}"
}

function test_increment_index_missing_args() {
	unset array_size
	unset index_to_increment
	error_code="13"
	(increment_index &> /dev/null)
	retcode="${?}"
	assert_raises "${retcode}" "${error_code}"
}

function test_decrement_index_proper() {
	dummy_idx="2"
	proper_answer="1"
	out_idx="$(decrement_index "${dummy_idx}")"
	assert_equals "${out_idx}" "${proper_answer}"
}

function test_decrement_index_min() {
	dummy_idx="0"
	proper_answer="0"
	out_idx="$(decrement_index "${dummy_idx}")"
	assert_equals "${out_idx}" "${proper_answer}"
}

function test_decrement_index_missing_arg() {
	unset index_to_decrement
	error_code="14"
	(decrement_index &> /dev/null)
	retcode="${?}"
	assert_raises "${retcode}" "${error_code}"
}

function test_does_proper_line_command() {
	dummy_line="dummy_line"
	proper_answer="1"
	out="$(does_proper_line "${dummy_line}")"
	assert_equals "${out}" "${proper_answer}"
}

function test_does_proper_line_sheabang() {
	dummy_line="#!/bin/bash"
	proper_answer="0"
	out="$(does_proper_line "${dummy_line}")"
	assert_equals "${out}" "${proper_answer}"
}

function test_does_proper_line_empty() {
	dummy_line=""
	error_code="0"
	(does_proper_line "${dummy_line}" &> /dev/null)
	retcode="${?}"
	assert_raises "${retcode}" "${error_code}"
}

function test_does_proper_line_no_args() {
	unset line
	error_code="0"
	proper_answer="0"
	(does_proper_line &> /dev/null)
	retcode="${?}"
	assert_raises "${retcode}" "${error_code}"
}

function test_get_line_from_file_proper() {
	dummy_number="1"
	proper_answer="#!/bin/bash"
	filename="$(basename "${0}")"
	out="$(get_line_from_file "${filename}" "${dummy_number}")"
	assert_equals "${out}" "${proper_answer}"
}

function test_get_line_from_file_bad_number() {
	dummy_number="-1"
	error_code="4"
	filename="$(basename "${0}")"
	(get_line_from_file "${filename}" "${dummy_number}" &> /dev/null)
	retcode="${?}"
	assert_raises "${retcode}" "${error_code}"
}

function test_get_line_from_file_missing_param() {
	unset num_of_line
	error_code="8"
	filename="$(basename "${0}")"
	(get_line_from_file "${filename}" &> /dev/null)
	retcode="${?}"
	assert_raises "${retcode}" "${error_code}"
}

function test_get_line_from_file_missing_args() {
	unset num_of_line
	unset file_name
	error_code="9"
	(get_line_from_file &> /dev/null)
	retcode="${?}"
	assert_raises "${retcode}" "${error_code}"
}

function test_check_input_values_missing_param() {
	unset exercise_script
	dummy_dir="ex01"
	error_code="12"
	(check_input_values "${dummy_dir}" &> /dev/null)
	retcode="${?}"
	assert_raises "${retcode}" "${error_code}"
}

function test_check_input_values_missing_args() {
	unset exercise_script
	unset exercise_dir
	error_code="11"
	(check_input_values &> /dev/null)
	retcode="${?}"
	assert_raises "${retcode}" "${error_code}"
}

function test_check_input_values_proper() {
	dummy_exercise_script="./ex01/01.sh"
	dummy_dir="ex01"
	out_code="0"
	(check_input_values "${dummy_dir}" "${dummy_exercise_script}" &> /dev/null)
	retcode="${?}"
	assert_raises "${retcode}" "${out_code}"
}

function test_execute_line_ignore_error() {
	dummy_command="ghd"
	out_code="0"
	(execute_line "${dummy_command}" &> /dev/null)
	retcode="${?}"
	assert_raises "${retcode}" "${out_code}"
}

function test_get_num_of_lines_from_file() {
	dummy_file="makefile"
	proper_answer="22"
	out="$(get_num_of_lines_from_file "${dummy_file}")"
	assert_equals "${out}" "${proper_answer}"
}

function test_get_begin_line_number_feature() {
	dummy_file="ex01/01.sh"
	proper_answer="6"
	out="$(get_begin_line_number "${dummy_file}")"
	assert_equals "${out}" "${proper_answer}"
}

function test_get_begin_line_number_no_feaure() {
	dummy_file="makefile"
	proper_answer="1"
	out="$(get_begin_line_number "${dummy_file}")"
	assert_equals "${out}" "${proper_answer}"
}

function test_get_begin_line_number_no_file() {
	dummy_file="dummy"
	out_code="16"
	(get_begin_line_number "${dummy_file}" &> /dev/null)
	retcode="${?}"
	assert_raises "${retcode}" "${out_code}"
}

function test_get_list_of_proper_lines_proper() {
	dummy_file="ex01/01.sh"
	proper_answer="8 11 14"
	out="$(get_list_of_proper_lines "${dummy_file}")"
	assert_equals "${out}" "${proper_answer}"
}

function test_get_list_of_proper_lines_no_file() {
	dummy_file="dummy"
	out_code="16"
	(get_list_of_proper_lines "${dummy_file}" &> /dev/null)
	retcode="${?}"
	assert_raises "${retcode}" "${out_code}"
}

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then

	declare -a TEST_CASES=(
		"die_with_err"
		"increment_index"
		"decerement_index"
		"does_proper_line"
		"get_line_from_file"
		"check_input_values"
		"execute_line"
		"get_num_of_lines_from_file"
		"get_begin_line_number"
		"get_list_of_proper_lines"
	)

	declare -A TESTS=(
		["test_die_with_error_proper"]="${TEST_CASES[0]}"
		["test_die_with_error_no_args"]="${TEST_CASES[0]}"
		["test_die_with_error_no_second"]="${TEST_CASES[0]}"
		["test_increment_index_proper"]="${TEST_CASES[1]}"
		["test_increment_index_max"]="${TEST_CASES[1]}"
		["test_increment_index_missing_size"]="${TEST_CASES[1]}"
		["test_increment_index_missing_args"]="${TEST_CASES[1]}"
		["test_decrement_index_min"]="${TEST_CASES[2]}"
		["test_decrement_index_proper"]="${TEST_CASES[2]}"
		["test_decrement_index_missing_arg"]="${TEST_CASES[2]}"
		["test_does_proper_line_empty"]="${TEST_CASES[3]}"
		["test_does_proper_line_sheabang"]="${TEST_CASES[3]}"
		["test_does_proper_line_command"]="${TEST_CASES[3]}"
		["test_does_proper_line_no_args"]="${TEST_CASES[3]}"
		["test_get_line_from_file_proper"]="${TEST_CASES[4]}"
		["test_get_line_from_file_bad_number"]="${TEST_CASES[4]}"
		["test_get_line_from_file_missing_param"]="${TEST_CASES[4]}"
		["test_get_line_from_file_missing_args"]="${TEST_CASES[4]}"
		["test_check_input_values_missing_param"]="${TEST_CASES[5]}"
		["test_check_input_values_missing_args"]="${TEST_CASES[5]}"
		["test_check_input_values_proper"]="${TEST_CASES[5]}"
		["test_execute_line_ignore_error"]="${TEST_CASES[6]}"
		["test_get_num_of_lines_from_file"]="${TEST_CASES[7]}"
		["test_get_begin_line_number_feature"]="${TEST_CASES[8]}"
		["test_get_begin_line_number_no_feaure"]="${TEST_CASES[8]}"
		["test_get_begin_line_number_no_file"]="${TEST_CASES[8]}"
		["test_get_list_of_proper_lines_proper"]="${TEST_CASES[9]}"
		["test_get_list_of_proper_lines_no_file"]="${TEST_CASES[9]}"
	)

	for test_case in "${TEST_CASES[@]}"; do
		echo -e "\n\tTEST CASE: ${test_case}"
		for key in "${!TESTS[@]}"; do
			if [ "${TESTS[${key}]}" == "${test_case}" ]; then
				echo "${key}"
				"${key}"
			fi
		done
	done

fi

set -e