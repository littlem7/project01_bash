#!/bin/bash

function echo_green() {
	echo -e "\033[1;32m${1}\033[0m"
}

function assert_error() {
	error_msg="${1}"
	error_code="${2}"

	echo -e "\033[0;31m[ERROR] ${error_msg}\033[0m"
	exit "${error_code}"
}

function assert_equals() {
	expected="${1}"
	actual="${2}"

	if [ "${expected}" != "${actual}" ]; then
		assert_error "ACTUAL=${actual} != EXPECTED=${expected}" "2"
	else
		echo_green "OK"
	fi
}	

function assert_raises() {
	expected_error_code="${1}"
	actual_error_code="${2}"

	if [ "${expected_error_code}" -ne "${actual_error_code}" ]; then
		assert_error "ACTUAL_ERROR=${actual_error_code} != EXPECTED_ERROR=${expected_error_code}" "2"
	else
		echo_green "OK"
	fi
}