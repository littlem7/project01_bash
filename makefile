list_of_directories := $(shell find . -name "ex[0-9]*" | sort)
list_of_exercises := $(list_of_directories:./ex%=%)
G='\033[0;32m'
LG='\033[1;32m'
B='\033[0;34m'
R='\033[0;31m'
NC='\033[0m'

sem:
	@clear
	@echo "Common info about exercises:"
	@for exercise in $(list_of_exercises); do \
		echo -n "$$exercise - "; \
		cat ex$$exercise/.info 2> /dev/null || echo "[no .info]"; \
	done

	@echo -e ${G}'\n'make xx${NC} - to call specialized exercise'\n'

%:
	@clear
	@echo "Call exercise $(@)"
	@./start.sh -e $(@)

.PHONY: sem
