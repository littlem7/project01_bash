#!/bin/bash
if [ -z "${EX_DIR}" ]; then
	EX_DIR=$(dirname "${BASH_SOURCE[0]}")
fi
# end of comment


# compile example
cd "${EX_DIR}"; make

# view for links at /usr/lib
ls -l `find /usr/lib -maxdepth 1 -type l -print`

# check SONAME for specialized version of library
objdump -p /usr/lib/libxview.so.3.2.4 | grep SONAME
