# Źródło #

Projekt został utworzony na podstawie narzędzia przygotowanego na przedmiot [Programowanie Niskopoziomowe](https://bitbucket.org/littlem7/programowanie_niskopoziomowe), który miał za zadanie prezentację kolejnych komend krokowo bez konieczności wpisywania ich ręcznie.

# Zmiany #

Projekt został rozwinięty w zakresie przemiotu Języki Skryptowe. Została dodana funkcjonalość przesuwania się po kolejnych komendach w przód i w tył oraz wykonywanie ich w dowolnym momencie. Dodatkowo rozbito skrypt na kilka funkcji, dla których przygotowano testy jednostkowe.

### Funkcjonalności ###
* skrypt parsuje skrypt o nazwie <parametr>.sh znajdujący się w katalogu ex<parametr> 
* plik .info w katalogu przykładu powinien zawierać krótką informację na temat przykładu
* skrypt parsując omija linie puste oraz wypełnione komentarzami
* dodatkowo zaimplementowano możliwość ukrycia pewnej partii komend; wystarczy je wpisać przed charakterystycznym komentarzem:

```
#!shell
# end of comment
```
* należy pamiętać, by używać zmiennej 
```
#!shell
EX_DIR
```
 zamiast 
```
#!shell
$(dirname "${BASH_SOURCE[0]}")
```
* jeżeli chcemy mieć możliwość uruchomienia przykładu standardowo, to najlepiej zmienną tę wyeksportować, jak zostało to podane w przykładach

* do ulatwienia pracy zostal stworzony makefile główny, który wywołuje skrypt z zadanym parametrem i wypisuje zawartosci .info

* wypisanie wszystkich przykładów wraz z krótkim opisem

```
#!shell
make
```
* uruchomienie przykladu 01 (na przykład)

```
#!shell
make 01
```
* uruchomienie reczne skryptu

```
#!shell
./start.sh -e 01
```
* uruchomienie reczne pomocy skryptu

```
#!shell
./start.sh -h
```

* uruchomienie testow jednostkowych

```
#!shell
./test_start.sh
```


### Tworzenie własnych przykładów ###

* należy stworzyć katalog ex<parametr>, a w nim skrypt <parametr.sh>, który będzie miał uprawnienia do wykonywania
* dobrze jest dodać informację o przykładzie w .info w katalogu ex<parametr>
* wpisać wybrane komendy (jednolinijkowe) do skryptu